name := "com.scalac.zadanie"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  "com.github.pureconfig" %% "pureconfig" % "0.10.2",
  "org.scalatest" %% "scalatest" % "3.0.4" % Test
)