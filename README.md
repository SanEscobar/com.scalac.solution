## About The Project
The application go over the provided input directory, read all 
images in format png or jpg, analyze them and write
them back to an output directory, that it is also provided.  The output and input 
directories are read from a config.
The copied images in the final directory include metadata, which 
 is coming from the analysis process and has two pieces of information:
- one of two labels: “dark” or “bright” depend on "cut off" point
- a score between 0 and 100 (inclusively) showing how dark the 
image is.

## Getting Started
Before starting the program, make sure the environmental
variables have been defined. Enter your settings in 
`application.conf`
```JS
path-in = "Your directory to the images"
path-out = "The path where the images with metadata will be kept"
```

The "cut off" point can be change in the `main.scala` class.
