import java.io.File

import com.pict.filter.{PictureCalculation, PictureReader, PictureWriter, ScalaConfig}
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.parallel.immutable.ParVector

class ApplicationTest extends FlatSpec with  Matchers{

  it should "Black picture file path" in new Context {
    imgs.map(names => names.getName.toString) contains (darkPictureOldName) shouldBe true
  }

  it should "Black picture mean brightness value" in new Context {
    rgbAvg shouldBe ParVector(0)
  }

  it should "Black picture normalised brightness" in new Context {
    val results = rgbAvg.par.map(x => pictCalculation.checkingBrightness(x)(cutOffPoint)).toList
    val normBrightness = rgbAvg.par.map(x => pictCalculation.getNormalisedBrightness(x)).toList
    normBrightness shouldBe List(100)
  }

  it should "copy Black picture in PathOUT" in new Context {
    val results = rgbAvg.par.map(x => pictCalculation.checkingBrightness(x)(cutOffPoint)).toList
    val normBrightness = rgbAvg.par.map(x => pictCalculation.getNormalisedBrightness(x)).toList
    val saveResult = pictWriter.fileNamesModyfying(imgs, results, normBrightness)
    saveResult.map(x=>x.newName.toString) shouldBe List(darkPictureNewName)
    pictWriter.SavingPicture(saveResult, pathOUT)
    pictReader.getListOfFiles(pathOUT).map(x=>x.toString) shouldBe  blackPictResponse
  }


 trait Context{
   val pathInBlack ="./com.scalac.zadanie/src/test/scala/testIN/dark"
   val pathOUT="./com.scalac.zadanie/src/test/scala/testOUT/"
   val darkPictureOldName = "black.jpg"
   val darkPictureNewName = "black_dark_100.jpg"
   implicit val cutOffPoint = 20
   val blackPictResponse = List((pathOUT+"black_dark_100.jpg").replace("/", File.separator))

   val pictReader = new PictureReader
   val pictCalculation = new PictureCalculation
   val pictWriter = new PictureWriter

   val imgs = pictReader.getListOfFiles(pathInBlack)
   val numImg = imgs.par.map(x => pictReader.pictureRead(x))
   val rgbVect = numImg.par.map(x => pictReader.pictureRGB(x))
   val rgbAvg = rgbVect.par.map(x => pictCalculation.getAvgBrigthness(x))
 }

}
