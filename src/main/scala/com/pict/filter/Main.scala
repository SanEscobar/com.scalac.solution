package com.pict.filter


object Main extends App {

  val scalaConfiguration = ScalaConfig.loadConfig
  val pictureReader = new PictureReader
  val pictureCalculation = new PictureCalculation
  val pictureWriter = new PictureWriter
  implicit val cutOffPoint = 20

  val imageFiles = pictureReader.getListOfFiles(scalaConfiguration.pathIo.pathIn)
  val averageValueOfPixels = imageFiles.par.map(x => pictureReader.pictureRead(x))
    .par.map(x => pictureReader.pictureRGB(x))
    .par.map(x => pictureCalculation.getAvgBrigthness(x))

  val results = averageValueOfPixels.par.map(x => pictureCalculation.checkingBrightness(x)).toList
  val normalisedBrightness = averageValueOfPixels.par.map(x => pictureCalculation.getNormalisedBrightness(x)).toList

  val saveMetaData = pictureWriter.fileNamesModyfying(imageFiles, results, normalisedBrightness)
  pictureWriter.SavingPicture(saveMetaData, scalaConfiguration.pathIo.pathOut)

}

