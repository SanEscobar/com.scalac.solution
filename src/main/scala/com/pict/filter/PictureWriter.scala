package com.pict.filter

import java.io.File
import java.nio.file.{Files, Paths, StandardCopyOption}


class PictureWriter {

  case class PictNames(oldName: String, newName: String)


  def fileNamesModyfying(fileNames: List[File], pictClassifiers: List[String], normBrightness: List[Int]) :List[PictNames] ={

    (fileNames, pictClassifiers, normBrightness)
      .zipped.toList
      .map(x=>PictNames(x._1.toString,x._1.toString.replaceAll(".*[\\\\\\/]", "")
        .replaceAll("\\.(?!.*\\.)","_" + x._2 + "_" + x._3 +"\\.")))
  }

  def SavingPicture(imgNames: List[PictNames], pathOut: String)={

   imgNames.map(x=> Files.copy(
      Paths.get(x.oldName),
      Paths.get(pathOut + x.newName),
      StandardCopyOption.REPLACE_EXISTING
    ))
  }


}
