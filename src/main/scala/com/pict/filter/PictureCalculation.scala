package com.pict.filter

class PictureCalculation {

  def getRedColour(rgb: Int): Int ={ (rgb>>16) & 0xff }
  def getGreenColour(rgb: Int): Int={(rgb>>8) & 0xff}
  def getBlueColour(rgb: Int): Int={rgb & 0xff}

  def getAvgBrigthness(rgbVect: Array[Int]): Int={
    (rgbVect.map(x => getRedColour(x)).sum + rgbVect.map(x => getGreenColour(x)).sum + rgbVect.map(x => getBlueColour(x)).sum)/(3*rgbVect.length)
  }

  def getNormalisedBrightness(avgB: Int): Int={
    100-(avgB.toFloat/255 * 100).round
  }

  def checkingBrightness(avgBrightness: Int)(implicit cutOffPoint: Int): String ={
   if(avgBrightness > cutOffPoint*255/100) "bright" else "dark"
  }


}
