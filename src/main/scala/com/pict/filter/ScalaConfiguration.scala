package com.pict.filter

import pureconfig.loadConfigOrThrow
import pureconfig.generic.auto._

case class ProjectConfiguration(pathIn: String, pathOut: String)
case class ProjectSettings(pathIo: ProjectConfiguration)

object ScalaConfig {
  def loadConfig = loadConfigOrThrow[ProjectSettings]("pict-filter")
}
