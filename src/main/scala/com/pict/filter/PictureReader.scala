package com.pict.filter

import java.io.File
import javax.imageio.ImageIO

class PictureReader{

  def getListOfFiles(dir: String):List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles
        .filter(_.isFile)
        .filter(name => name.getName.endsWith("jpg") ||
        name.getName.endsWith("png") ||
        name.getName.endsWith("jpeg")).toList
    } else {
      List[File]()
    }
  }

  def pictureRead(path: File): java.awt.image.BufferedImage={
     ImageIO.read(path)
  }

  def pictureRGB(pict: java.awt.image.BufferedImage): Array[Int] ={
    val w = pict.getWidth()
    val h = pict.getHeight()
    val dataBuffInt = pict.getRGB(0, 0, w, h, null, 0, w)
    dataBuffInt
  }
}
